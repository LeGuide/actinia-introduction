# actinia-introduction

Introduction to actinia cloud geoprocessing (https://actinia.mundialis.de/)

## How to build locally

First (and only once):

```
pip install mkdocs setuptools
```

Then (everytime you want the build to run):

```
mkdocs build
```

Open `site/index.html` in your favourite browser to see the output.
